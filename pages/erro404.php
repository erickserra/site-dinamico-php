<section class="erro">
	<div class="container" style="padding:90px 0; text-align: center;">
		<hr>
		<h2 style="font-weight: normal;"><i class="fas fa-times"></i>&nbsp;A Pagina Não existe ! </h2>
		<p>Deseja voltar para a <a href="<?php echo INCLUDE_PATH; ?>">pagina inicial?</a></p>
	</div><!--container-->
</section><!--erro-->