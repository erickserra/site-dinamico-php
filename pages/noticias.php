<?php
	$url = explode('/',$_GET['url']);
	if (!isset($url[2])) {
		$categorias = MySql::conectar()->prepare("SELECT * FROM `tb_site_categorias` WHERE slug = ?");
		$categorias->execute(array(@$url[1]));
		if ($categorias->rowCount() == 1) {
			$certo = true;
			$categorias = $categorias->fetch();
		}else{
			$certo = false;
		}
?>
<section class="section-noticias">
	<div class="container">

		<div class="header-noticias">
			<h2><i class="fas fa-bullhorn"></i></h2>
			<h2>Acompanhe as últimas notícias do <b>portal</b></h2>
		</div><!--header-noticias-->
	</div><!--container-->
</section><!--section-noticias-->

<section class="container-portal">
	<div class="container">
		<div class="sidebar">
			<div class="box-content-sidebar">
				<h3>Realizar uma busca: <i class="fas fa-search"></i></h3>
				<form method="POST">	
					<input type="text" name="parametro" placeholder="O que deseja procurar?" required>
					<input type="submit" name="acao" value="Buscar">
				</form>
			</div><!--box-content-sidebar-->
			<div class="box-content-sidebar">
				<h3>Selecione a categoria: <i class="fas fa-list-ul"></i></h3>
				<form>
					<select name="categoria">
						<option value="" selected>Todas as categorias</option>
						<?php 
							$categoria = MySql::conectar()->prepare("SELECT * FROM `tb_site_categorias` ");
							$categoria->execute();
							$categoria = $categoria->fetchAll(PDO::FETCH_ASSOC);
							foreach ($categoria as $key => $value) {
						?>
							<option <?php if($value['slug'] == @$url[1]){ echo 'selected'; } ?> value="<?php echo $value['slug']; ?>"><?php echo $value['nome']; ?></option>
						<?php } ?>
					</select>
				</form>
			</div><!--box-content-sidebar-->
			<div class="box-content-sidebar">
				<h3>Sobre o autor: <i class="fas fa-user"></i></h3>	
				<div class="autor-box-portal">
					<?php 
						$boxAutor = MySql::conectar()->prepare("SELECT * FROM `tb_site_config` ");
						$boxAutor->execute();
						$boxAutor = $boxAutor->fetch();
					?>
					<div class="box-img-autor" style="background-image: url('<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $boxAutor['foto_autor'] ?>');"></div><!--box-img-autor-->
					<div class="texto-autor-portal">
						<?php 
							$infoAutor = MySql::conectar()->prepare("SELECT * FROM `tb_site_config` ");
							$infoAutor->execute();
							$infoAutor = $infoAutor->fetch();
						?>
						<h3><?php echo $infoAutor['nome_autor'];  ?></h3>
						<p><?php echo substr($infoAutor['descricao'],0,600); ?> ...</p>
					</div><!--texto-autor-portal-->
				</div><!--autor-box-portal-->
			</div><!--box-content-sidebar-->
	</div><!--sidebar-->
	<div class="conteudo-portal">
		<div class="header-conteudo-portal">
			<?php
				$porPagina = 10;

				if ($certo) {
					echo "<h2>Vizualizando Posts em <span>".$categorias['nome']."</span></h2>";
				}else{
					echo '<h2>Vizualizando Todos os Posts</h2>';
				}

				$query = "SELECT * FROM `tb_site_noticias` ";
				if ($certo){
					$categorias['id'] = (int)$categorias['id'];
					$query.="WHERE categoria_id = $categorias[id] ";
				}
				if (isset($_POST['acao']) && isset($_POST['parametro'])) {
					if (strstr($query,'WHERE') !== false) {
						$busca = $_POST['parametro'];
						$query.=" AND titulo LIKE '%$busca%' ";
					}else{
						$busca = $_POST['parametro'];
						$query.=" WHERE titulo LIKE '%$busca%' ";
					}
				}
				if (isset($_GET['pagina'])) {
					$pagina = (int)$_GET['pagina'];
					$queryPg = ($pagina - 1) * $porPagina;
					$query.=" ORDER BY id DESC LIMIT $queryPg,$porPagina ";
				}else{
					$pagina = 1;
					$query.=" ORDER BY id DESC LIMIT 0,$porPagina ";
				}
				$sql = MySql::conectar()->prepare($query);
				$sql->execute();
				$noticias = $sql->fetchAll();
			?>
		</div><!--header-conteudo-portal-->
		<?php
			foreach ($noticias as $key => $value) {
			$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_categorias` WHERE id = ? ");
			$sql->execute(array($value['categoria_id']));
			$categoriaNome = $sql->fetch()['slug'];
		?>
		<div class="box-single-conteudo">
			<h2><?php echo date("d/m/Y",strtotime($value['data'])); ?> - <?php echo $value['titulo']; ?></h2>
			<p><?php echo substr(strip_tags($value['conteudo']),0,400); ?> ...</p>
			<a href="<?php echo INCLUDE_PATH; ?>noticias/<?php echo $categoriaNome; ?>/<?php echo $value['slug']; ?>">Leia mais</a>
		</div><!--box-single-conteudo-->
		<?php } ?>	
		<?php
			
			$query = "SELECT * FROM `tb_site_noticias`";
			if ($certo){
				$categorias['id'] = (int)$categorias['id'];
				$query.="WHERE categoria_id = $categorias[id] ";
			}
			$totalPaginas = MySql::conectar()->prepare($query);
			$totalPaginas->execute();
			$totalPaginas = ceil($totalPaginas->rowCount() / $porPagina);
		 ?>
		<div class="paginator">
			<?php
				for ($i=1; $i <= $totalPaginas ; $i++) {  
					$catStr = ($certo == true) ? '/'.$categorias['slug'] : '';
					if ($pagina == $i) {
						echo '<a class="active-page" href="'.INCLUDE_PATH.'noticias'.$catStr.'?pagina='.$i.'">'.$i.'</a>';
					}else{
						echo '<a href="'.INCLUDE_PATH.'noticias'.$catStr.'?pagina='.$i.'">'.$i.'</a>';
					}
				}
			?>
		</div><!--paginator-->
	</div><!--conteudo-portal-->
	<div class="clear"></div>
	</div><!--container-->
</section><!--container-portal-->

<?php }else{
	include('noticia-single.php');
	}
?>

