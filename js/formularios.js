$(function(){

	$('body').on('submit','form.ajax-form',function(){
		var form = $(this);
		$.ajax({
			beforeSend:function(){
				$('.overlay-loading').fadeIn();
			},
			url:include_path+'formularios.php',
			method:'post',
			dataType:'json',
			data:form.serialize()
		}).done(function(data){
			$('.alert').fadeIn();
			$('.alert[pag=contato]').fadeIn();
			setTimeout(function(){
				$('.alert').fadeOut();
				$('.alert[pag=contato]').fadeOut();
			},2000);
			$('.overlay-loading').fadeOut();
			$('form input[type=email]').val(' ');
			$('form input[type=text]').val(' ');
			$('form textarea').val(' ');
		});
		return false; 
	});



})