<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Cadastrar Categoria</h2>
		<div class="form-editar-usuario">

			<?php
				
				if (isset($_POST['acao'])) {
					$nome = $_POST['nome'];
					if ($nome != '') {
						$verifica = MySql::conectar()->prepare("SELECT * FROM `tb_site_categorias` WHERE nome = ?");
						$verifica->execute(array($nome));
						if ($verifica->rowCount() == 1) {
							Painel::alertBox('erro','Já existe uma categoria com esse nome');
						}else{
							$slug = Painel::generateSlug($nome);
							$arr = ['nome'=>$nome,'slug'=>$slug,'order_id'=>'0','nome_tabela'=>'tb_site_categorias'];
							Painel::insert($arr);
							Painel::alertBox('sucesso','Categoria cadastrada com sucesso!');
						}
					}else{
						Painel::alertBox('erro','Campos vazios não são permitidos !');
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome</label>
					<input type="text" name="nome" required">
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="order_id" value="0">
					<input type="hidden" name="nome_tabela" value="tb_site_categorias">
					<input type="submit" name="acao" value="Cadastrar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->