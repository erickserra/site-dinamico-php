<?php
	
	if (isset($_GET['excluir'])) {
		$idExcluir = (int)$_GET['excluir'];
		$sql = MySql::conectar()->prepare("SELECT capa FROM `tb_site_noticias` WHERE id = ? ");
		$sql->execute(array($_GET['excluir']));
		$imagem = $sql->fetch();
		Painel::deleteFile($imagem['capa']);
		Painel::deletar("tb_site_noticias",$idExcluir);
		Painel::redirect(INCLUDE_PATH_PAINEL.'gerenciar-noticias');
	}else if(isset($_GET['order']) && isset($_GET['id']) ){
		Painel::orderItem('tb_site_noticias',$_GET['order'],$_GET['id']);
	}

	$paginaAtual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1; 
	$porPagina = 4;
	$noticias = Painel::selectAll('tb_site_noticias',($paginaAtual - 1) * $porPagina,$porPagina);
?>

<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-equals"></i>Notícias Cadastradas</h2>
		<div class="wraper-table">
			<div class="table-responsive">
				<div class="row">
					<div class="col col-4">
						<span>Título</span>
					</div><!--col-->
					<div class="col col-4">
						<span>Capa</span>
					</div><!--col-->
					<div class="col col-4">
						<span>Categoria</span>
					</div><!--col-->
					<div class="col col-4">
						<span>Conteúdo</span>
					</div><!--col-->
					<div class="col col-4">
						<span>Editar</span>
					</div><!--col-->
					<div class="col col-4">
						<span>Excluir</span>
					</div><!--col-->
					<div class="col col-x">
						<span>UP</span>
					</div><!--col-->
					<div class="col col-x">
						<span>DOWN</span>
					</div><!--col-->
				</div><!--row-->
				<?php

					foreach ($noticias as $key => $value) {
					$nomeCategoria = Painel::select('tb_site_categorias','id=?',array($value['categoria_id']))['nome'];
				?>
					<div class="row">
						<div class="col col-4">
							<span><?php echo $value['titulo']; ?></span>
						</div><!--col-->
						<div class="col col-4">
							<img width="50" height="50" src="<?php echo INCLUDE_PATH_PAINEL ?>uploads/<?php echo $value['capa']; ?>">
						</div><!--col-->	
						<div class="col col-4">
							<span><?php echo $nomeCategoria; ?></span>
						</div><!--col-->
						<div class="col col-4">
							<span><?php echo $value['conteudo'] ?></span>
						</div><!--col-->
						<div class="col col-4 icon-tabel">	
							<span><a href="<?php echo INCLUDE_PATH_PAINEL?>editar-noticia?id=<?php echo $value['id']?>"><i class="fas fa-pencil-alt"></i> Editar</a></span>
						</div><!--col-->
						<div class="col col-4 icon-tabel">	
							<span><a actionExcluir="delete" style="background-color: #ef5350;" href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-noticias?excluir=<?php echo $value['id']?>"><i class="fas fa-times"></i>Excluir</a></span>
						</div><!--col-->
						<div class="col col-x">
							<a href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-noticias?order=up&id=<?php echo $value['id']?>"><i class="fas fa-angle-up"></i></a>
						</div><!--col-->
						<div class="col col-x">
							<a href="<?php echo INCLUDE_PATH_PAINEL ?>gerenciar-noticias?order=down&id=<?php echo $value['id']?>"><i class="fas fa-angle-down"></i></a>
						</div><!--col-->
					</div><!--row-->
				<?php } ?>
			</div><!--table-responsive-->
		</div><!--wraper-table-->
		<div class="pagination">
			<?php
				$totalPaginas = ceil(count(Painel::selectAll('tb_site_noticias')) / $porPagina);
				if ($totalPaginas != 1) {
					for ($i=1; $i <= $totalPaginas; $i++) { 
						if ($i == $paginaAtual)
							echo '<a class="page-active" href="'.INCLUDE_PATH_PAINEL.'gerenciar-noticias?pagina='.$i.'">'.$i.'</a>';
						else
							echo '<a href="'.INCLUDE_PATH_PAINEL.'gerenciar-noticias?pagina='.$i.'">'.$i.'</a>';
					}
				}
			?>
		</div><!--pagination-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->