<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Editar Slide</h2>
		<div class="form-editar-usuario">

			<?php

				$id = isset($_GET['id']) ? $_GET['id'] : false;

				if ($id == false) {
					Painel::alertBox('erro','Vc precisa passar o parametro ID');
					die();
				}

				$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_slides` WHERE id = $id ");
				$sql->execute();
				$result = $sql->fetch(PDO::FETCH_ASSOC);

				if (isset($_POST['acao'])) {
					$nome = $_POST['nome'];
					$img = $_FILES['slide'];
					$img_atual = $result['slide'];
					if ($img['name'] != '') {

						if (Painel::validarImagem($img)){

							Painel::deleteFile($img_atual);
							$img = Painel::uploadFile($img);

							$arr = ['nome_tabela'=>'tb_site_slides','id'=>$id,'nome'=>$nome,'slide'=>$img];

							if(Painel::update($arr) == true)
								Painel::alertBox('sucesso','Slide atualizado com sucesso');
							else
								Painel::alertBox('erro','não foi possivel fazer a alteração');
						}else{

							Painel::alertBox('erro','formato do arquivo errado');
						}
					}else{
						$arr = ['nome_tabela'=>'tb_site_slides','id'=>$id,'nome'=>$nome,'slide'=>$img_atual];

						if(Painel::update($arr) == true)
							Painel::alertBox('sucesso','Slide atualizado com sucesso');
						else
							Painel::alertBox('erro','não foi possivel fazer a alteração');
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome : </label>
					<input type="text" name="nome" value="<?php echo $result['nome']; ?>" required>
				</div><!--form-group-->
				<div class="form-group">
					<label>Imagem : </label>
					<input type="file" name="slide">
				</div><!--form-group-->	
				<div class="form-group">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->