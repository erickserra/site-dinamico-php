
<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Editar Categoria</h2>
		<div class="form-editar-usuario">

			<?php

				$id = isset($_GET['id']) ? $_GET['id'] : false;

				if ($id == false) {
					Painel::alertBox('erro','Um erro foi encontrado');
					die();
				}

				$sql = MySql::conectar()->prepare("SELECT * FROM `tb_site_categorias` WHERE id = $id ");
				$sql->execute();
				$result = $sql->fetch(PDO::FETCH_ASSOC);

				if (isset($_POST['acao'])) {

					$check = MySql::conectar()->prepare("SELECT * FROM `tb_site_categorias` WHERE nome = ? AND  id != ? ");
					$check->execute(array($_POST['nome'],$id));
					if ($check->rowCount() == 1) {
						Painel::alertBox('erro','Já existe uma categoria com este nome !');
					}else{
						$slug = Painel::generateSlug($_POST['nome']);
						$arr = array_merge($_POST,array('slug'=>$slug));
						if (Painel::update($arr)) {
							$result = Painel::select('tb_site_categorias','id=?',array($result['id']));
							Painel::alertBox('sucesso','O cadastro do depoimento foi realizado com sucesso');
						}else{
							Painel::alertBox('erro','Campos vazios não são permitidos.');
						}
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome: </label>
					<input type="text" name="nome" value="<?php echo $result['nome']; ?>">
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $result['id'] ?>"> 	
					<input type="hidden" name="nome_tabela" value="tb_site_categorias">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->