<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Cadastrar Slides</h2>
		<div class="form-editar-usuario">

			<?php

				if (isset($_POST['acao'])) {
					//enviei o formulario
			
					$nome = $_POST['nome'];
					$slide = $_FILES['slide'];
					
					if($slide['name'] != ''){
						//existe o upload de imagem.
						if(Painel::validarImagem($slide)){
							include('../classes/lib/WideImage.php');
							$slide = Painel::uploadFile($slide);
							WideImage::load('uploads/'.$slide)->resize(1920)->saveToFile('uploads/'.$slide);
							$sql = MySql::conectar()->prepare("INSERT INTO `tb_site_slides` VALUES(NULL,?,?,?) ");
							$sql->execute(array($nome,$slide,0));
							$order_id = MySql::conectar()->lastInsertId();
							$sql = MySql::conectar()->prepare("UPDATE `tb_site_slides` SET order_id = ? WHERE id = $order_id ");
							if ($sql->execute(array($order_id))){
								Painel::alertBox('sucesso','slide cadastrado com sucesso!');
							}
							else{
								Painel::alertBox('erro','Ocorreu um erro ao cadastrar o slide');
							}
						}else{
							Painel::alertBox("erro","Imagem invalida | Verifique o tamanho da imagem ou o formato do arquivo");
						}
					}else{
						Painel::alertBox("erro","Imagem não encontrada");
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome</label>
					<input type="text" name="nome" required">
				</div><!--form-group-->
				<div class="form-group">
					<label>Imagem</label>
					<input type="file" name="slide">
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="order_id" value="0">
					<input type="hidden" name="nome_tabela" value="tb_site_slides">
					<input type="submit" name="acao" value="Cadastrar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->