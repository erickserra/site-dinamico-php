<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Editar Usuário</h2>
		<div class="form-editar-usuario">

			<?php
				if (isset($_POST['acao'])) {
					//enviei o formulario
			
					$newNome = $_POST['nome'];
					$newPassword = $_POST['password'];
					$newImg = $_FILES['imagem'];
					$imagem_atual = $_POST['imagem_atual'];
					
					if($newImg['name'] != ''){
						//existe o upload de imagem.
						if(Painel::validarImagem($newImg)){
							$usuario = new Usuario();
							$newImg = Painel::uploadFile($newImg);
							if ($usuario->atualizarUsuario($newNome,$newPassword,$newImg)){

							Painel::alertBox("sucesso","Atualizado com sucesso junto com a imagem !");
							//Painel::deleteFile($imagem_atual);
							
							$_SESSION['img'] = $newImg;
							$_SESSION['nome'] = $newNome;

							}else{
								Painel::alertBox("erro","Ocorreu um erro ao atualizar. junto com a imagem.");
							}
						}else{
							Painel::alertBox("erro","Imagem invalida | Verifique o tamanho da imagem ou o formato do arquivo");
						}
					}else{
						$newImg = $imagem_atual;
						$usuario = new Usuario();
						if ($usuario->atualizarUsuario($newNome,$newPassword,$newImg)) {
							Painel::alertBox("sucesso","Atualizado com sucesso !");
						}else{
							Painel::alertBox("erro","Ocorreu um erro ao atualizar..");
						}
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Nome</label>
					<input type="text" name="nome" required value="<?php echo $_SESSION['nome'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Senha</label>
					<input type="password" name="password" required value="<?php echo $_SESSION['password'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Imagem</label>
					<input type="file" name="imagem" value="<?php echo $_SESSION['img'] ?>">
					<input type="hidden" name="imagem_atual" value="<?php echo $_SESSION['img'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->