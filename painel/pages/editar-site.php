	<?php
	$infoSite = MySql::conectar()->prepare("SELECT * FROM `tb_site_config` ");
	$infoSite->execute();
	$infoSite = $infoSite->fetch();
?>

<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Configurações do site</h2>
		<div class="form-editar-usuario">

			<?php
				if (isset($_POST['acao'])) {
					
					$foto_autor = $_FILES['foto_autor'];

					$foto_atual = MySql::conectar()->prepare("SELECT `foto_autor` FROM `tb_site_config`");
					$foto_atual->execute();
					$foto_atual = $foto_atual->fetch();

					if ($foto_autor['name'] != '') {
						if (Painel::validarImagem($foto_autor)) {
							include('../classes/lib/WideImage.php');   
							$foto_autor = Painel::uploadFile($foto_autor);
							Painel::deleteFile($foto_atual);	
							WideImage::load('uploads/'.$foto_autor)->resize(300,400)->saveToFile('uploads/'.$foto_autor);
							$sql = MySql::conectar()->prepare("UPDATE `tb_site_config` SET foto_autor = ? ");
							$sql->execute(array($foto_autor));
							if (Painel::update($_POST,true)) {
								Painel::alertBox('sucesso','Campos modificados com sucesso ! ');
							}else{
								Painel::alertBox('erro','Campos vazios não são permitidos.');
							}
						}else{
							Painel::alertBox('erro','Imagem com formato ou tamanho em demasia');
						}
					}else{
						if (Painel::update($_POST,true)) {
							Painel::alertBox('sucesso','Campos modificados com sucesso ! ');
						}else{
							Painel::alertBox('erro','Campos vazios não são permitidos.');
						}
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Título do site</label>
					<input type="text" name="titulo" value="<?php echo $infoSite['titulo']; ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Nome do Autor</label>
					<input type="text" name="nome_autor" value="<?php echo $infoSite['nome_autor']; ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Foto de Perfil</label>
					<input type="file" name="foto_autor">
				</div><!--form-group-->
				<div class="form-group">
					<label>Descrição</label>
					<textarea name="descricao"><?php echo $infoSite['descricao']; ?></textarea>
				</div><!--form-group-->	
				<div class="form-group">
					<label>icone 1</label>
					<input type="text" name="icone_1" value="<?php echo $infoSite['icone_1']; ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Título 1</label>
					<input type="text" name="titulo_1" value="<?php echo $infoSite['titulo_1']; ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Descrição 1</label>
					<textarea name="descricao_1"><?php echo $infoSite['descricao_1'] ?></textarea>
				</div><!--form-group-->
				<div class="form-group">
					<label>icone 2</label>
					<input type="text" name="icone_2" value="<?php echo $infoSite['icone_2'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Título 2</label>
					<input type="text" name="titulo_2" value="<?php echo $infoSite['titulo_2'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Descrição 2</label>
					<textarea name="descricao_2"><?php echo $infoSite['descricao_2'] ?></textarea>
				</div><!--form-group-->
				<div class="form-group">
					<label>icone 3</label>
					<input type="text" name="icone_3" value="<?php echo $infoSite['icone_3'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Título 3</label>
					<input type="text" name="titulo_3" value="<?php echo $infoSite['titulo_3'] ?>">
				</div><!--form-group-->
				<div class="form-group">
					<label>Descrição 3</label>
					<textarea name="descricao_3"><?php echo $infoSite['descricao_3'] ?></textarea>
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="nome_tabela" value="tb_site_config">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->