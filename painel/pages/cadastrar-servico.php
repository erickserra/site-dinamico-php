<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Adicionar Serviço</h2>
		<div class="form-editar-usuario">

			<?php
				if (isset($_POST['acao'])) {
					if (Painel::insert($_POST)) {
						Painel::alertBox('sucesso','O cadastro do depoimento foi realizado com sucesso');
					}else{
						Painel::alertBox('erro','Campos vazios não são permitidos.');
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Serviço</label>
					<textarea name="servico"></textarea>
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="order_id" value="0">
					<input type="hidden" name="nome_tabela" value="tb_admin_servicos">
					<input type="submit" name="acao" value="Cadastrar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->