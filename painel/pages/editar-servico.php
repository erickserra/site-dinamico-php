
<div class="box-content w100">
	<div class="box-content-wraper">
		<h2><i class="fas fa-pencil-alt"></i>Editar Serviços</h2>
		<div class="form-editar-usuario">

			<?php

				$id = isset($_GET['id']) ? $_GET['id'] : false;

				if ($id == false) {
					Painel::alertBox('erro','Um erro foi encontrado');
					die();
				}

				$sql = MySql::conectar()->prepare("SELECT * FROM `tb_admin_servicos` WHERE id = $id ");
				$sql->execute();
				$result = $sql->fetch(PDO::FETCH_ASSOC);

				if (isset($_POST['acao'])) {
					$newServico = $_POST['servico'];
					if (Painel::update($_POST)) {
						Painel::alertBox('sucesso','O cadastro do depoimento foi realizado com sucesso');
					}else{
						Painel::alertBox('erro','Campos vazios não são permitidos.');
					}
				}
			?>

			<form method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label>Serviço : </label>
					<textarea name="servico"><?php echo $result['servico'] ?> </textarea>
				</div><!--form-group-->
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $result['id'] ?>">
					<input type="hidden" name="nome_tabela" value="tb_admin_servicos">
					<input type="submit" name="acao" value="Atualizar">
				</div><!--form-group-->
			</form>
		</div><!--form-editar-usuario-->
	</div><!--box-content-wraper-->
</div> <!--box-content-->