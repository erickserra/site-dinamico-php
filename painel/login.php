<?php 
	
	if (isset($_COOKIE['lembrar'])) {
		$user = $_COOKIE['user'];
		$password = $_COOKIE['password'];
		$pdo = MySql::conectar();	
		$sql = $pdo->prepare("SELECT * FROM `tb_admin` WHERE user = ? AND password = ?");
		$sql->execute(array($user,$password));
		if ($sql->rowCount() == 1) {
			$info = $sql->fetch();
			$_SESSION['login'] = true;
			$_SESSION['user'] = $user;
			$_SESSION['password'] = $password;
			$_SESSION['cargo'] = $info['cargo'];
			$_SESSION['nome'] = $info['nome'];
			$_SESSION['img'] = $info['img'];
			header('Location:'.INCLUDE_PATH_PAINEL);
			die();
		}
	}
	

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo INCLUDE_PATH_PAINEL; ?>css/login.css">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0">
	<meta name="X-UA-Compatible" content="IE=Edge">
	<title>login</title>
</head>
<body>
	<div class="box-login">
		<?php

			if (isset($_POST['acao'])) {

				$user = $_POST['user'];
				$password = $_POST['password'];

				$pdo = MySql::conectar();	
				$sql = $pdo->prepare("SELECT * FROM `tb_admin` WHERE user = ? AND password = ?");
				$sql->execute(array($user,$password));
				//echo '<script>alert("'.$_POST['user'].'")</script>';
				if ($sql->rowCount() == 1) {
					//logamos com sucesso
					$info = $sql->fetch();

					$_SESSION['login'] = true;
					$_SESSION['user'] = $user;
					$_SESSION['password'] = $password;
					$_SESSION['cargo'] = $info['cargo'];
					$_SESSION['nome'] = $info['nome'];
					$_SESSION['img'] = $info['img'];
					/*
					if (isset($_POST['lembrar'])) {
						setcookie('lembrar',true,time() + (60*60*24),'/');
						setcookie('user',$user,time() + (60*60*24),'/');
						setcookie('password',$password,time() + (60*60*24),'/');
					}
					*/
					header('Location:'.INCLUDE_PATH_PAINEL);
					die();
				}else{
					//login falhou
					echo '<div class="box-erro" ><p><i class="fa fa-times"></i>Usuário ou senha incorretos.</p></div>';
				}

			}

		?>
		<div class="title-login">
			<h2>SIGN IN</h2>
			<div class="line-title-login"></div>
		</div>
		<form method="post" action="">
			<p>USERNAME</p>
			<input type="text" name="user" required>
			<p>PASSWORD</p>
			<input type="password" name="password"  required>
			<input type="submit" name="acao" value="SIGN IN">
			<div class="form-check">
				<label>Lembrar-me</label>
				<input type="checkbox" name="lembrar">
			</div>
		</form>
		<div class="footer-login">
			<p>Forgot your password?</p>
			<div class="line-footer"></div>
		</div>
	</div><!--box-login-->
</body>
</html>