<?php

	//Iniciando Sessao
	session_start();
	date_default_timezone_set("America/Sao_Paulo");

	
	//CONSTANTS DIRECTORY
	//define('INCLUDE_PATH','http://localhost/Projetos PHP/projeto_01/');
	define('INCLUDE_PATH','http://localhost/Projetos PHP/projeto_01/');
	define('INCLUDE_PATH_PAINEL',INCLUDE_PATH.'painel/');
	define('BASE_DIR_PAINEL',__DIR__.'/painel/');

	//CONSTANTES MySql
	define('HOST','localhost');
	define('DBNAME','Projeto_01');
	define('USER','root');
	define('PASSWORD','');

	//variaveis de email
	$host = 'smtp.gmail.com';
	$username = 'erickjoaoteste@gmail.com';
	$password = "<?senha>";
	$name = 'Erick';

	//autoload
	$autoload = function($class){
			require_once('classes/'.$class.'.php');
	};

	spl_autoload_register($autoload);

	//Funçoes do Painel

	function pegaCargo($indice){
		return Painel::$cargos[$indice];
	}

	function selecionadoMenu($parametro){
		/*<i class="fas fa-angle-double-right"></i>*/
		$url = explode('/',@$_GET['url'])[0];
		if ($url == $parametro) {
			echo 'class="menu-active"';
		}
	}

	function verificaPermissaoMenu($permissao){
		if ($_SESSION['cargo'] >= $permissao) {
			return;
		}else{
			echo 'style="display:none;"';
		}
	}

	function verificaPermissaoPagina($permissao){
		if ($_SESSION['cargo'] >= $permissao) {
			return;
		}else{
			include('painel/pages/permissao_negada.php');
			die();
		}
	}
	//metricas do site

?>

